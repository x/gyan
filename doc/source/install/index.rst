===============================
gyan service installation guide
===============================

.. toctree::
   :maxdepth: 2

   get_started.rst
   install.rst
   verify.rst
   next-steps.rst

The gyan service (gyan) provides...

This chapter assumes a working setup of OpenStack following the
`OpenStack Installation Tutorial
<https://docs.openstack.org/project-install-guide/ocata/>`_.
