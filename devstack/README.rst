====================
DevStack Integration
====================

This directory contains the files necessary to integrate gyan with devstack.

Refer the quickstart guide at
https://docs.openstack.org/gyan/latest/contributor/quickstart.html
for more information on using devstack and gyan.
