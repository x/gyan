# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import copy
import signal
import sys
import gyan.conf

CONF = gyan.conf.CONF

non_negative_integer = {
    'type': ['integer', 'string'],
    'pattern': '^[0-9]*$', 'minimum': 0
}

positive_integer = {
    'type': ['integer', 'string'],
    'pattern': '^[0-9]*$', 'minimum': 1
}

boolean_extended = {
    'type': ['boolean', 'string'],
    'enum': [True, 'True', 'TRUE', 'true', '1', 'ON', 'On', 'on',
             'YES', 'Yes', 'yes',
             False, 'False', 'FALSE', 'false', '0', 'OFF', 'Off', 'off',
             'NO', 'No', 'no'],
}

boolean = {
    'type': ['boolean', 'string'],
    'enum': [True, 'True', 'true', False, 'False', 'false'],
}

ml_model_name = {
    'type': ['string', 'null'],
    'minLength': 2,
    'maxLength': 255,
    'pattern': '^[a-zA-Z0-9][a-zA-Z0-9_.-]+$'
}

flavor_name = {
    'type': ['string', 'null'],
    'minLength': 2,
    'maxLength': 255,
    'pattern': '^[a-zA-Z0-9][a-zA-Z0-9_.-]+$'
}

flavor_cpu = {
    'type': ['number', 'integer', 'null'],
    'minLength': 2,
    'maxLength': 255
}

flavor_driver = {
    'type': ['string', 'null'],
    'minLength': 2,
    'maxLength': 255,
    'pattern': '^[a-zA-Z0-9][a-zA-Z0-9_.-]+$'
}

flavor_disk = {
    'type': ['string', 'null'],
    'minLength': 2,
    'maxLength': 255,
}

flavor_memory = {
    'type': ['string', 'null'],
    'minLength': 2,
    'maxLength': 255
}

flavor_additional_details = {
    'type': ['object', 'null'],
    'minLength': 2,
    'maxLength': 255
}

flavor_python_version = {
    'type': ['string', 'null', 'number', 'integer'],
    'minLength': 2,
    'maxLength': 255
}

hex_uuid = {
    'type': 'string',
    'maxLength': 32,
    'minLength': 32,
    'pattern': '^[a-fA-F0-9]*$'
}


labels = {
    'type': ['object', 'null']
}

hints = {
    'type': ['object', 'null']
}
hostname = {
    'type': ['string', 'null'],
    'minLength': 2,
    'maxLength': 63
}

repo = {
    'type': 'string',
    'minLength': 2,
    'maxLength': 255,
    'pattern': '[a-zA-Z0-9][a-zA-Z0-9_.-]'
}



string_ps_args = {
    'type': ['string'],
    'pattern': '[a-zA-Z- ,+]*'
}

str_and_int = {
    'type': ['string', 'integer', 'null'],
}

hostname = {
    'type': 'string', 'minLength': 1, 'maxLength': 255,
    # NOTE: 'host' is defined in "services" table, and that
    # means a hostname. The hostname grammar in RFC952 does
    # not allow for underscores in hostnames. However, this
    # schema allows them, because it sometimes occurs in
    # real systems.
    'pattern': '^[a-zA-Z0-9-._]*$',
}

ml_model_name = {
    'type': 'string',
    'minLength': 1,
    'maxLength': 255,
    'pattern': '^[a-zA-Z0-9-._]*$'
}

ml_model_flavor_id = {
    'type': 'string',
    'minLength': 1,
    'maxLength': 255,
    'pattern': '^[a-zA-Z0-9-._]*$'
}

ml_model_type = {
    'type': 'string',
    'minLength': 1,
    'maxLength': 255,
    'pattern': '^[a-zA-Z0-9-._]*$'
}
