# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ALLOCATED = 'allocated'
CREATED = 'created'
UNDEPLOYED = 'undeployed'
DEPLOYED = 'deployed'
CREATING = 'CREATING'
CREATED = 'CREATED'
SCHEDULED = 'SCHEDULED'
DEPLOYED_COMPUTE_NODE = 'DEPLOYED COMPUTE NODE'
DEPLOYMENT_FAILED = 'DEPLOYMENT FAILED'
DEPLOYMENT_STARTED = 'DEPLOYMENT STARTED'
NAME_PREFIX='gyan-'